package date.example;


import java.text.SimpleDateFormat;
import java.util.Date;

// 格式化时间（SimpleDateFormat）
public class DateFormat {
    public static void main(String[] args) {
        Date date = new Date();
        String strDateFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strDateFormat);
        System.out.println(simpleDateFormat.format(date));
    }
}
