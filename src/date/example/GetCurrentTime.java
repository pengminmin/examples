package date.example;

import java.text.SimpleDateFormat;
import java.util.Date;

// 获取当前时间
public class GetCurrentTime {
    public static void main(String[] args) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss a");// a为am/pm的标记
        Date date = new Date();
        System.out.println("现在时间：" + simpleDateFormat.format(date));

    }
}
