package function.example;

// 方法重载
public class MainClass {
    public static void main(String[] args) {
        MyClass myClass = new MyClass(3);
        myClass.info();
        myClass.info("重载方法");
        // 重载构造函数
        new MyClass();
    }
}

class MyClass{
    int height;

    MyClass(){
        System.out.println("无参构造函数");
        height = 4;
    }

    MyClass(int i){
        System.out.println("房子的高度为：" + i + " 米");
        height = i;
    }

    void info(){
        System.out.println("房子的高度为：" + height + " 米");
    }

    void info(String s){
        System.out.println(s + "：房子的高度为：" + height + " 米");
    }
}
