package string.example;
// 删除字符串中的一个字符
public class RemoveCharAt {
    public static void main(String[] args) {
        String s = "this is java";
        System.out.println(removeCharAt(s, 3));
    }

    public static String removeCharAt(String s, int pos) {
        return s.substring(0, pos) + s.substring(pos + 1);
    }
}
