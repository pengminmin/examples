package string.example;
// 查找字符串最后一次出现的位置
public class SearchLastString {
    public static void main(String[] args) {
        String s = "hello world";
        int lastIndex = s.lastIndexOf("wo");
        if (lastIndex == -1) {
            System.out.println("no");
        } else {
            System.out.println("yes " + lastIndex);
        }
    }
}
